<?php
  require_once("model.php");
  
  if(isset($_POST["register"])) {
    $password = password_hash($_POST["pass"], PASSWORD_DEFAULT);
    $email = htmlspecialchars($_POST["email"]);
    $name = htmlspecialchars($_POST["name"]);
    $phone = htmlspecialchars($_POST["phone"]);
    $city = htmlspecialchars($_POST["city"]);
    $psc = htmlspecialchars($_POST["psc"]);
    $adress = htmlspecialchars($_POST["adress"]);
    
    if ($_POST["pass"] == $_POST["passagain"]) {
      $loginname = register();
      if($loginname==null) {
        $regerror = "Problém s registrací.";
      }
      $_SESSION["logged"] = True;
      $_SESSION["username"] = $loginname;
      header("Location: main.php");  
    } else {
      $regerror = "Hesla nejsou stejná";
    }
  }
?>