<?php 
  session_start();
  
  require("registercontroller.php");
  require("logincontroller.php");
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="windows-1250">
    <title>Computer4U</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
    <header>
      <h1><a href="about.php">Computer4U</a></h1>
      <h2>Váš obchod s PC sestavami<h2>
    </header>
<?php
  if (!isset($_SESSION["logged"])) {
?>
    <div id="login">
      <form method="POST" name="login">
        <p>
          <label for="username">Jméno:</label>
          <input type="text" name="username" required value="<?php if (isset($_POST["login"])) {echo $_POST["username"]; }; ?>">
        </p>
        <p>
          <label for="password">Heslo:</label>
          <input type="password" name="password" required>
        </p>
        <input type="submit" value="Přihlásit" name="login" id="btLogin">
        <button type="button" onclick="parent.location='register.php'" id="btRegister">Registrace</button>
      </form>
    </div>
<?php 
  };
  if (isset($_SESSION["logged"])) { 
    echo "<p>Přihlášen jako: ".$_SESSION["username"]."</p>";
?>
    <div>
			<button type="button" onclick="parent.location='logout.php'">Odhlášení</button>
		</div>
<?php    
  }; 
  if (isset($_SESSION["logged"])) {
?>
    <h5>Již jste zaregistrovaný.</h5>
<?php 
  }; 
  if (!isset($_SESSION["logged"])) {
?>
    <div id="registrace">
      <h3>Registrace nového uživatele</h3>
      <form method="POST" name="register">
        <p>
          <label for="name">Jméno:</label>
          <input type="text" name="name" required value="<?php if (isset($_POST["register"])) {echo $_POST["name"]; }; ?>">
          <label for="pass">Heslo:</label>
          <input type="password" name="pass" required >
        </p>
        <p>
          <label for="passagain">Heslo znovu:</label>
          <input type="password" name="passagain" required>
          <label for="email">Email:</label>
          <input type="text" name="email" required>
        </p>
        <p>
          <label for="phone">Tel. číslo:</label>
          <input type="text" name="phone" required value="<?php if (isset($_POST["register"])) {echo $_POST["phone"]; }; ?>">
          <label for="city">Město:</label>
          <input type="text" name="city" required value="<?php if (isset($_POST["register"])) {echo $_POST["city"]; }; ?>">
        </p>
        <p>
          <label for="psc">PSČ:</label>
          <input type="text" name="psc" required value="<?php if (isset($_POST["register"])) {echo $_POST["psc"]; }; ?>">
          <label for="adress">Adresa:</label>
          <input type="text" name="adress" required value="<?php if (isset($_POST["register"])) {echo $_POST["adress"]; }; ?>">
        </p>
<?php 
    if (isset($regerror)) {
      echo $regerror;
    };   
?>
          <img>
          <input type="checkbox" name="agree" required>
          <label for="agree">Souhlasím s právními podmínkami</label>
        <p>
          <input type="submit" value="Zaregistrovat" name="register">
        </p>
      </form>
    </div>
<?php
  };
?>
    <footer>
      <p>CVUT FEL 2017</p>
    </footer>
  </body>
</html>