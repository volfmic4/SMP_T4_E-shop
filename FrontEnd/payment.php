<?php 
  session_start();
  
  require("logincontroller.php");
  require("purchasecontroller.php");
  require("customcontroller.php");
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="windows-1250">
    <title>Computer4U</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
    <header>
      <h1><a href="about.php">Computer4U</a></h1>
      <h2>Váš obchod s PC sestavami<h2>
    </header>
<?php
  if (!isset($_SESSION["logged"])) {
?>
    <div id="login">
      <form method="POST" name="login">
        <p>
          <label for="username">Jméno:</label>
          <input type="text" name="username" required value="<?php if (isset($_POST["login"])) {echo $_POST["username"]; }; ?>">
        </p>
        <p>
          <label for="password">Heslo:</label>
          <input type="password" name="password" required>
        </p>
        <input type="submit" value="Přihlásit" name="login" id="btLogin">
        <button type="button" onclick="parent.location='register.php'" id="btRegister">Registrace</button>
      </form>
    </div>
<?php 
  };
  if (isset($_SESSION["logged"])) { 
    echo "<p>Přihlášen jako: ".$_SESSION["username"]."</p>";
?>
    <div>
			<button type="button" onclick="parent.location='logout.php'">Odhlášení</button>
		</div>
<?php    
  };
  if(!isset($_POST["buy"])) {
?>
    <div id="nakup">
    <form method="POST" name="payment">
      <div id="platba">
        <p>
          <h3>Vyberte způsob platby</h3>
          <input type="radio" name="pay" value="card" required>
          <label>Kartou</label>
          <input type="radio" name="pay" value="transfer">
          <label>Převodem</label>
          <input type="radio" name="pay" value="personal">
          <label>Na dobírku</label>
        </p>
      </div>
      <div id="doprava">
        <p>
          <h3>Vyberte způsob dopravy</h3>
          <input type="radio" name="delivery" value="personal" required>
          <label>Osobní převzetí</label>
          <input type="radio" name="delivery" value="post">
          <label>Poštovní služba</label>
          <input type="radio" name="delivery" value="parcel">
          <label>Balíková služba</label>
        </p>   
      </div>
      <div id="informace">
<?php
    if(!isset($_SESSION["logged"])) {
?>        
        <p>
          <label>Jméno:</label>
          <input type="text" name="name" required>
          <label>Adresa:</label>
          <input type="text" name="adress" required>
        </p>
        <p>
          <label>Tel. číslo:</label>
          <input type="text" name="phone" required>
          <label>Město:</label>
          <input type="text" name="city" required>
        </p>
        <p>
          <label>PSČ:</label>
          <input type="text" name="psc" required>
<?php 
    };
?>
          <input type="submit" value="Zakoupit" name="buy">
        </p>
      </div>
    </form>
<?php
  } else {
?>  
    <div id="potvrzeni">
      <p>Položka: <?php echo $_SESSION["item"]; ?></p>
      <p>Cena: <?php echo $_SESSION["price"]; ?> Kč</p>
      <p>Doručení: <?php echo $delivery; ?></p>
      <p>Platba: <?php echo $payment; ?></p>
      <p>Jméno: <?php echo $name; ?></p>
      <p>Adresa: <?php echo $adress; ?></p>
      <p>Tel. Číslo: <?php echo $phone; ?></p>
      <p>Město: <?php echo $city; ?></p>
      <p>PSČ: <?php echo $psc; ?></p>
    </div>  
<?php  
  };
?>
    </div>
    <footer>
      <p>CVUT FEL 2017</p>
    </footer>>
  </body>
</html>