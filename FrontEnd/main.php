<?php 
  session_start();
  
  require("logincontroller.php");
  require("purchasecontroller.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="windows-1250">
    <title>Computer4U</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
    <header>
      <h1><a href="about.php">Computer4U</a></h1>
      <h2>Váš obchod s PC sestavami<h2>
    </header>
<?php
  if (!isset($_SESSION["logged"])) {
?>
    <div id="login">
      <form method="POST" name="login">
        <p>
          <label for="username">Jméno:</label>
          <input type="text" name="username" required value="<?php if (isset($_POST["login"])) {echo $_POST["username"]; }; ?>">
        </p>
        <p>
          <label for="password">Heslo:</label>
          <input type="password" name="password" required>
        </p>
        <input type="submit" value="Přihlásit" name="login" id="btLogin">
        <button type="button" onclick="parent.location='register.php'" id="btRegister">Registrace</button>
      </form>
    </div>
<?php 
  };
  if (isset($_SESSION["logged"])) { 
    echo "<p>Přihlášen jako: ".$_SESSION["username"]."</p>";
?>
    <div>
			<button type="button" onclick="parent.location='logout.php'" id="btLogout">Odhlášení</button>
		</div>
<?php    
  };
?>
    <section class="products">
      <div id="sestava1" class="items">
        <h3>Office4U</h3>
        <img src="./img/office.jpg">
        <ul>
          <li>Cena: 10 000 Kč</li>
          <li>Procesor: ...</li>
          <li>Harddisk: ...</li>
          <li>Zdroj: ...</li>
          <li>Grafická karta: ...</li>
          <form method="GET" name="office" action="payment.php">
            <input type="submit" value="Zakoupit" name="office">
          </form>
        </ul>  
      </div>
      <div id="sestava2" class="items">
        <h3>Media4U</h3>
        <img src="./img/media.jpg">
        <ul>
          <li>Cena: 15 000 Kč</li>
          <li>Procesor: ...</li>
          <li>Harddisk: ...</li>
          <li>Zdroj: ...</li>
          <li>Grafická karta: ...</li>
          <form method="GET" name="media" action="payment.php">
            <input type="submit" value="Zakoupit" name="media">
          </form>
        </ul>  
      </div>
      <div id="sestava3" class="items">
        <h3>Gaming4U</h3>
        <img src="./img/gaming.png">
        <ul>
          <li>Cena: 20 000 Kč</li>
          <li>Procesor: ...</li>
          <li>Harddisk: ...</li>
          <li>Zdroj: ...</li>
          <li>Grafická karta: ...</li>
          <form method="GET" name="gaming" action="payment.php">
            <input type="submit" value="Zakoupit" name="gaming">
          </form>
        </ul>  
      </div>
      <div id="sestava4" class="items">
        <h3>High-End4U</h3>
        <img src="./img/highend.jpg">
        <ul>
          <li>Cena: 25 000 Kč</li>
          <li>Procesor: ...</li>
          <li>Harddisk: ...</li>
          <li>Zdroj: ...</li>
          <li>Grafická karta: ...</li>
          <form method="GET" name="highend" action="payment.php">
            <input type="submit" value="Zakoupit" name="high">
          </form>
        </ul>  
      </div>
      <div id="sestava5" class="items">
        <h3>Custom4U</h3>
        <img src="./img/custom.jpg">
        <ul>
          <li>Cena: vlastní</li>
          <li>Procesor: vlastní</li>
          <li>Harddisk: vlastní</li>
          <li>Zdroj: vlastní</li>
          <li>Grafická karta: vlastní</li>
          <button type="button" onclick="parent.location='custom.php'">Sestavit</button>
        </ul>  
      </div>
    </section>  
    <footer>
      <p>CVUT FEL 2017</p>
    </footer>
  </body>
</html>
