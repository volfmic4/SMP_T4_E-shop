<?php
  session_start();
	
	if(isset($_SESSION["logged"])) {
		unset($_SESSION["logged"]);
		unset($_SESSION["username"]);
    unset($_SESSION["name"]);
	}
	header("Location: main.php");
?>