<?php
  function register(){
    return "Josef Novák";
  }
  
  function login() {
    return "Josef Novák";
  }
  
  function getWares() {
    if(isset($_POST["procesor"])) {
      return array("procesor1","procesor2","procesor3","procesor4");
    } elseif (isset($_POST["motherboard"])) {
      return array("motherboard1","motherboard2","motherboard3","motherboard4");
    } elseif (isset($_POST["graphics"])) {
      return array("graphics1","graphics2","graphics3","graphics4");
    } elseif (isset($_POST["power"])) {
      return array("power1","power2","power3","power4");
    } elseif (isset($_POST["drives"])) {
      return array("drives1","drives2","drives3","drives4");
    } elseif (isset($_POST["case"])) {
      return array("case1","case2","case3","case4");
    } elseif (isset($_POST["ram"])) {
      return array("ram1","ram2","ram3","ram4");
    } elseif (isset($_POST["other"])) {
      return array("other1","other2","other3","other4");
    } else {
      return null;
    }
  }
?>