<?php 
  session_start();
  
  require("logincontroller.php");
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="windows-1250">
    <title>Computer4U</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
    <header>
      <h1><a href="about.php">Computer4U</a></h1>
      <h2>Váš obchod s PC sestavami<h2>
    </header>
<?php
  if (!isset($_SESSION["logged"])) {
?>
    <div id="login">
      <form method="POST" name="login">
        <p>
          <label for="username">Jméno:</label>
          <input type="text" name="username" required value="<?php if (isset($_POST["login"])) {echo $_POST["username"]; }; ?>">
        </p>
        <p>
          <label for="password">Heslo:</label>
          <input type="password" name="password" required>
        </p>
        <input type="submit" value="Přihlásit" name="login" id="btLogin">
        <button type="button" onclick="parent.location='register.php'" id="btRegister">Registrace</button>
      </form>
    </div>
<?php 
  };
  if (isset($_SESSION["logged"])) { 
    echo "<p>Přihlášen jako: ".$_SESSION["username"]."</p>";
?>
    <div>
			<button type="button" onclick="parent.location='logout.php'">Odhlášení</button>
		</div>
<?php    
  };
?>
    <div id="map">
      <img src="img/map.png">
    </div>
    <div id="about">
      <h3>O nás</h3>
      <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris luctus, tortor ut elementum feugiat, nibh nisi laoreet eros, et lacinia orci lectus in orci. Nulla eu tristique sem. Pellentesque sed diam neque. Fusce metus nibh, euismod vel vehicula eget, euismod vel tortor. Sed iaculis justo augue, fringilla congue lorem varius vel. Praesent tempor eu lorem eu convallis. Nam at risus non magna tincidunt fringilla. Proin eget nibh mi.</h4>
      <h3>Kontakt</h3>
      <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris luctus, tortor ut elementum feugiat, nibh nisi laoreet eros, et lacinia orci lectus in orci. Nulla eu tristique sem. Pellentesque sed diam neque. Fusce metus nibh, euismod vel vehicula eget, euismod vel tortor. Sed iaculis justo augue, fringilla congue lorem varius vel. Praesent tempor eu lorem eu convallis. Nam at risus non magna tincidunt fringilla. Proin eget nibh mi.</h4>
    </div>
    <footer>
      <p>CVUT FEL 2017</p>
    </footer>
  </body>
</html>
