<?php 
  session_start();
  
  require("logincontroller.php");
  require("customcontroller.php");
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="windows-1250">
    <title>Computer4U</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
    <header>
      <h1><a href="about.php">Computer4U</a></h1>
      <h2>Váš obchod s PC sestavami<h2>
    </header>
<?php
  if (!isset($_SESSION["logged"])) {
?>
    <div id="login">
      <form method="POST" name="login">
        <p>
          <label for="username">Jméno:</label>
          <input type="text" name="username" required value="<?php if (isset($_POST["login"])) {echo $_POST["username"]; }; ?>">
        </p>
        <p>
          <label for="password">Heslo:</label>
          <input type="password" name="password" required>
        </p>
        <input type="submit" value="Přihlásit" name="login" id="btLogin">
        <button type="button" onclick="parent.location='register.php'" id="btRegister">Registrace</button>
      </form>
    </div>
<?php 
  };
  if (isset($_SESSION["logged"])) { 
    echo "<p>Přihlášen jako: ".$_SESSION["username"]."</p>";
?>
    <div>
			<button type="button" onclick="parent.location='logout.php'">Odhlášení</button>
		</div>
<?php    
  };
?>
    <div id="custom">
      <h5>Celková cena: <?php echo $_SESSION["total"]; ?>Kč</h5>
      <form method="POST" name="reset" action="payment.php">
        <input type="submit" value="Zaplatit" name="reset">
      </form>
      <div id="menu">
        <form method="POST" name="procesor">
          <input type="submit" value="Procesory" name="procesor">
        </form>
        <form method="POST" name="motherboard">
          <input type="submit" value="Základní desky" name="motherboard">
        </form>
        <form method="POST" name="graphics">
          <input type="submit" value="Grafické karty" name="graphics">
        </form>
        <form method="POST" name="power">
          <input type="submit" value="Zdroje" name="power">
        </form>
        <form method="POST" name="drives">
          <input type="submit" value="HDD/SSD" name="drives">
        </form>
        <form method="POST" name="case">
          <input type="submit" value="Skříně" name="case">
        </form>
        <form method="POST" name="ram">
          <input type="submit" value="RAM" name="ram">
        </form>
        <form method="POST" name="other">
          <input type="submit" value="Ostatní" name="other">
        </form>
      </div>
      <div id="items">
<?php
  $wares = getWares();
  if ($wares == null) {
    echo "Vyberte prosím položku";
  } else {
  for($i=0;$i<count($wares);$i++) {
?>
        <p>
          <img>
          <h5><?php echo $wares[$i]; ?></h5>
          <form method="POST" name="choose1">
            <input type="submit" value="Vybrat" name="<?php echo $i; ?>">
          </form>
        </p>    
<?php
  }}
?>        

      </div>
    </div>
    <footer>
      <p>CVUT FEL 2017</p>
    </footer>
  </body>
</html>