<?php
  require_once("model.php");
  
  if (isset($_POST["login"])) {
    $username = htmlspecialchars($_POST["username"]);
    if ($username == "") { //Hl�d� nepr�zdnost username
      $error = "Zadejte jm�no";
    }
    $password = htmlspecialchars($_POST["password"]); //Hl�d� nepr�zdnost hesla
    if ($_POST["password"] == "") {
      $error = "Zadejte heslo";
    }
    
    $loginname = login();
    if($loginname==null) {
      $error = "Probl�m s p�ihl�en�m.";
    }
    $_SESSION["logged"] = True;
    $_SESSION["username"] = $loginname;
  }
?>